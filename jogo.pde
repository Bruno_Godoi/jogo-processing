
int s = second();
void setup() {
  size(640, 360);
  frameRate(30);
  noStroke();

}


int raio = 50;
float tempo = millis();

void DrawCirculo(PImage src, float raio,int centrox, int centroy) {
  for(float i = 0; i<360 ; i = i + 0.05) {
    int x = (int) (centrox + (raio * cos(i)));
    int y = (int) (centroy + (raio * sin(i)));
    int pos = (y)*src.width + (x); /* acessa o ponto em forma de vetor */
    src.pixels[pos] = color(0, 0, 0);
  }
  src.updatePixels();
}


void time() {
  PFont words = createFont("Vernada", 12);
  fill(#000000); 
  textAlign(CENTER);
  textFont(words, 50);
  text(second(), 350, 175); 
  text(":", 300, 175);
  text(minute(), 250, 175);
  text(":", 200, 175);
  text(hour(), 150, 175);
}

void draw() {
    PImage arena = createImage(640, 360, RGB); 
    for (int y = 0; y < arena.height; y++) {
        for (int x = 0; x < arena.width; x++) {
          int pos = (y)*arena.width + (x); /* acessa o ponto em forma de vetor */
          arena.pixels[pos] = color(255, 0, 0);
       }
    }
    DrawCirculo(arena,raio,arena.width/2,arena.height/2);
    if( millis() - tempo  > 200 ) {
     raio = raio -1;
     arena.updatePixels();
     tempo = millis();
    }
    image(arena,0,0);
}